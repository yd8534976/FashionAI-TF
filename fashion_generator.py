from fashion_helper import *
import cv2
import os


class DataLoader(object):

    def __init__(self, data_dir='train_fashion', mode='train'):
        self.curr = 0
        self.mode = mode
        if mode == 'train':
            self.img_paths, self.labels = prepare_data(data_dir=data_dir, is_shuffle=True)
        else:
            self.img_paths, self.labels = prepare_test_paths(test_dir=data_dir)
        self.n = len(self.img_paths)

    def generator(self, n=0):
        i = 0
        if n == 0:
            n = self.n
        while i < n:
            img_path = self.img_paths[i]
            label = self.labels[i]

            img = read_and_pad(img_path)

            heatmap = label_to_heatmap(label, img)
            # heatmap = cv2.resize(heatmap, (512, 512))

            if self.mode == 'train':
                img_rotate, label_rotate = self.rotate(img, label)
                heatmap_rotate = label_to_heatmap(label_rotate, img_rotate)
                yield img_path, img_rotate, heatmap_rotate

                img_flip, heatmap_flip = self.flip(img, heatmap)
                yield img_path, img_flip, heatmap_flip
                # heatmap_hard = self.hard_points(heatmap)
                # yield img_path, num_visible, img, heatmap_hard

            yield img_path, img, heatmap
            if self.mode == 'test':
                img = img[:, ::-1, :]
                yield img_path, img, heatmap
            i += 1

    @staticmethod
    def hard_points(heatmap):
        new_heatmap = np.zeros_like(heatmap)
        idx_hard_points = [5, 6, 7, 8, 19]
        new_heatmap[:, :, idx_hard_points] = heatmap[:, :, idx_hard_points]
        return new_heatmap

    @staticmethod
    def rotate(img, label):
        angle = np.random.uniform(-30, 30)
        center = (256, 256)
        rot_mat = cv2.getRotationMatrix2D(center=center, angle=angle, scale=1.0)
        new_img = cv2.warpAffine(img, rot_mat, (512, 512))
        new_label = np.copy(label)
        for i in range(24):
            new_label[i, :2] = np.dot(rot_mat[:, :2], label[i, :2]) + rot_mat[:, 2]
        return new_img, new_label

    @staticmethod
    def flip(img, heatmap):
        new_img = img[:, ::-1, :]
        new_heat = flip_to_origin(heatmap)

        return new_img, new_heat
