import tensorflow as tf


def shift(name,
          inputs,
          ksize=3,
          dilation_rate=1,
          custom_getter=None
):
    """
    Shift operation. see paper: https://arxiv.org/abs/1711.08141
    :param name: str, Scope name
    :param inputs: tf.Tensor, format: NHWC
    :param ksize: odd int, kernel size to define shift directions
    :param dilation_rate: int, dilation rate
    :param custom_getter: Callable that takes as a first argument the true getter,
        and allows overwriting the internal get_variable method.
    :return: tf.Tensor, same shape as inputs
    """

    assert ksize % 2 != 0

    with tf.variable_scope(name_or_scope=name, custom_getter=custom_getter):
        _, h, w, c = inputs.shape
        h = h.value
        w = w.value
        c = c.value

        num_groups = ksize ** 2
        group_size = c // num_groups

        # if number of channels is less than number of direction,
        #   we directly return 3x3conv results
        if group_size == 0:
            outputs = tf.layers.conv2d(inputs,
                                       c,
                                       kernel_size=(3, 3),
                                       padding="same",
                                       activation=None)
            return outputs

        pad_size = ksize // 2
        pad_size *= dilation_rate
        paddings = tf.constant([[0, 0], [pad_size, pad_size], [pad_size, pad_size], [0, 0]])
        x = tf.pad(inputs, paddings)

        # get dilated kernel size
        ksize = (ksize - 1) * dilation_rate + 1

        curr_group_idx = 0
        end_channel_idx = 0

        list_out = []
        for i in range(0, ksize, dilation_rate):
            for j in range(0, ksize, dilation_rate):
                start_channel_idx = curr_group_idx * group_size
                end_channel_idx = (curr_group_idx + 1) * group_size
                curr_group_idx += 1
                list_out.append(x[:, i:i+h, j:j+w, start_channel_idx:end_channel_idx])
        list_out.append(x[:, 0:h, 0:w, end_channel_idx:])
        outputs = tf.concat(list_out, 3)

    return outputs


def conv_shift_conv(name,
                    inputs,
                    out_channels,
                    ksize,
                    expansion=1,
                    dilation_rate=1,
                    is_training=tf.constant(True),
                    activation=tf.nn.relu,
                    use_bias=True,
                    use_batch_norm=True,
                    is_debug=False,
                    data_format='channels_last',
                    custom_getter=None
):
    """
    Conv-Shift-Conv Block, see paper: https://arxiv.org/abs/1711.08141
    :param name: str, Block name
    :param inputs: tf.Tensor, Inputs
    :param out_channels: int, Number of output channels
    :param ksize: odd int, kernel size to define shift directions
    :param expansion: int, expansion rate, mid_channels = expansion * out_channels
    :param dilation_rate: int, dilation rate
    :param is_training: tf.constant, Flag if training or not
    :param activation: callable, Activation function
    :param use_bias: bool, If use bias
    :param use_batch_norm: bool, If use batch norm
    :param is_debug: bool, If is debug
    :param data_format: str, channels_last for NHWC, channels_first for NCHW, Default is channels_last
    :param custom_getter: callable, Custom getter for 'tf.variable_scope'
    :return: tf.Tensor, Output of CSC block
    """
    four_letter_data_format = 'NHWC' if data_format == 'channels_last' else 'NCHW'
    with tf.variable_scope(name_or_scope=name, custom_getter=custom_getter):
        mid_channels = expansion * out_channels
        conv1 = tf.layers.conv2d(inputs,
                                 mid_channels,
                                 name='conv1',
                                 kernel_size=(1, 1),
                                 activation=None,
                                 use_bias=False,
                                 data_format=data_format)
        if use_batch_norm:
            batch_normed1 = tf.contrib.layers.batch_norm(conv1,
                                                         decay=0.3,
                                                         scale=True,
                                                         center=True,
                                                         updates_collections=None,
                                                         is_training=is_training,
                                                         data_format=four_letter_data_format)
        else:
            batch_normed1 = conv1

        if use_bias:
            bias1 = tf.get_variable('bias1', shape=mid_channels, initializer=tf.zeros_initializer)
            biased1 = batch_normed1 + bias1
        else:
            biased1 = batch_normed1

        if activation:
            activated1 = activation(biased1)
        else:
            activated1 = biased1

        shifted = shift(name='shift',
                        inputs=activated1,
                        ksize=ksize,
                        dilation_rate=dilation_rate,
                        custom_getter=custom_getter)

        conv2 = tf.layers.conv2d(shifted,
                                 out_channels,
                                 name='conv2',
                                 kernel_size=(1, 1),
                                 activation=None,
                                 use_bias=False,
                                 data_format=data_format)

        if use_batch_norm:
            batch_normed2 = tf.contrib.layers.batch_norm(conv2,
                                                         decay=0.99,
                                                         scale=True,
                                                         center=True,
                                                         updates_collections=None,
                                                         is_training=is_training,
                                                         data_format=four_letter_data_format)
        else:
            batch_normed2 = conv2

        if use_bias:
            bias2 = tf.get_variable('bias2', shape=out_channels, initializer=tf.zeros_initializer)
            biased2 = batch_normed2 + bias2
        else:
            biased2 = batch_normed2

        if activation:
            output = activation(biased2)
        else:
            output = biased2

        if is_debug:
            tf.summary.histogram('conv1', conv1)
            tf.summary.histogram('batch_normed1', batch_normed1)
            tf.summary.histogram('biased1', biased1)
            tf.summary.histogram('activated1', activated1)
            tf.summary.histogram('shifted', shifted)
            tf.summary.histogram('conv2', conv2)
            tf.summary.histogram('batch_normed2', batch_normed2)
            tf.summary.histogram('biased2', biased2)
            tf.summary.histogram('output', output)

    return output


def shift_block(inputs,
                name="block",
                out_channels=64,
                num_units=3,
                strides=2,
                expansion=1,
                dilation_rate=1,
                is_training=True):
    with tf.variable_scope(name_or_scope=name):
        out = tf.layers.conv2d(inputs=inputs,
                               filters=out_channels,
                               kernel_size=(3, 3),
                               strides=strides,
                               padding="same")
        for i in range(num_units):
            skip = out
            out = conv_shift_conv(name="csc_{}".format(i),
                                  inputs=out,
                                  out_channels=out_channels,
                                  ksize=3,
                                  expansion=expansion,
                                  dilation_rate=dilation_rate,
                                  is_training=is_training)
            out += skip
    return inputs


def resnet_shift(inputs,
                 is_training=True,
                 expansion=1,
                 dilation_rate=1,
                 reuse=None,
                 scope='resnet_shift'):
    """
    resnet50 shift version for dense prediction
    :param inputs: Tensor, (None, H, W, C)
    :param is_training: bool, for batch_norm
    :param expansion: int, expansion rate for shift
    :param dilation_rate: int, dilation rate
    :param reuse: bool
    :param scope: str
    :return: out, Tensor, (None, H, W, 2048)
             endpoints, list of Tensor
    """
    endpoints = {}
    with tf.variable_scope(name_or_scope=scope, reuse=reuse):
        out = tf.layers.conv2d(inputs,
                               name="conv1",
                               filters=64,
                               kernel_size=(7, 7),
                               strides=2,
                               padding="same")
        endpoints["conv1"] = out
        out = shift_block(out,
                          'block1',
                          out_channels=256,
                          num_units=3,
                          strides=2,
                          expansion=expansion,
                          dilation_rate=dilation_rate,
                          is_training=is_training)
        endpoints["block1"] = out
        out = shift_block(out,
                          'block2',
                          out_channels=512,
                          num_units=4,
                          strides=2,
                          expansion=expansion,
                          dilation_rate=dilation_rate,
                          is_training=is_training)
        endpoints["block2"] = out
        out = shift_block(out,
                          'block3',
                          out_channels=1024,
                          num_units=6,
                          strides=2,
                          expansion=expansion,
                          dilation_rate=dilation_rate,
                          is_training=is_training)
        endpoints["block3"] = out
        out = shift_block(out,
                          'block4',
                          out_channels=2048,
                          num_units=4,
                          strides=2,
                          expansion=expansion,
                          dilation_rate=dilation_rate,
                          is_training=is_training)
        endpoints["block4"] = out
    return out, endpoints
